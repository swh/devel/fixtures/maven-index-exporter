#!/bin/bash

# Copyright (C) 2021-2022 The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

DOCKER_IMAGE=${1-"softwareheritage/maven-index-exporter"}
LOG=test_docker_image.log

REPO_DIR=$(CDPATH= cd -- "$(dirname -- "$0")/.." && pwd)
WORK_DIR=$REPO_DIR/tests/repository_test
EXPORT_DIR=$WORK_DIR/export
PUBLISH_DIR=$REPO_DIR/tests/publish

# clean up publish directory
rm -rf $PUBLISH_DIR/*
mkdir -p $PUBLISH_DIR

# This will mock out the download part of the python code
export MVN_IDX_EXPORTER_BASE_URL='test://example.org'
# Run the image on the maven indexes.
docker run -v $WORK_DIR:/work \
       -v $PUBLISH_DIR:/publish \
       -e MVN_IDX_EXPORTER_BASE_URL \
       $DOCKER_IMAGE >>$LOG 2>&1

# Assert exported text files are there, with the correct content.
EXPORT_FILE=$(ls $EXPORT_DIR/*.fld)
if [[ -e $EXPORT_FILE ]]; then
    echo "PASS: file [$EXPORT_FILE] has been created."
else
    echo "FAIL: file [$EXPORT_FILE] has NOT been created."
    exit 20
fi

DOCS=$(grep -E "^doc" $EXPORT_FILE | wc -l)
if [[ $DOCS -eq 10 ]]; then
    echo "PASS: file [$EXPORT_FILE] has 10 docs."
else
    echo "FAIL: file [$EXPORT_FILE] has $DOCS docs, should be 10."
    exit 20
fi

FIELDS=$(grep -E "^  field" $EXPORT_FILE | wc -l)
if [[ $FIELDS -eq 35 ]]; then
    echo "PASS: file [$EXPORT_FILE] has 35 fields."
else
    echo "FAIL: file [$EXPORT_FILE] has $FIELDS fields, should be 35."
    exit 20
fi

FIELDS=$(grep "value al.aldi|sprova4j|0.1.0|sources|jar" $EXPORT_FILE | wc -l)
if [[ $FIELDS -eq 1 ]]; then
    echo "PASS: file [$EXPORT_FILE] has sprova4j-0.1.0-sources.jar."
else
    echo "FAIL: file [$EXPORT_FILE] has NOT sprova4j-0.1.0-sources.jar."
    exit 20
fi

FIELDS=$(grep "value al.aldi|sprova4j|0.1.0|NA|pom" $EXPORT_FILE | wc -l)
if [[ $FIELDS -eq 1 ]]; then
    echo "PASS: file [$EXPORT_FILE] has sprova4j-0.1.0.pom."
else
    echo "FAIL: file [$EXPORT_FILE] has NOT sprova4j-0.1.0.pom."
    exit 20
fi

FIELDS=$(grep "value al.aldi|sprova4j|0.1.1|sources|jar" $EXPORT_FILE | wc -l)
if [[ $FIELDS -eq 1 ]]; then
    echo "PASS: file [$EXPORT_FILE] has sprova4j-0.1.1-sources.jar."
else
    echo "FAIL: file [$EXPORT_FILE] has NOT sprova4j-0.1.1-sources.jar."
    exit 20
fi

FIELDS=$(grep "value al.aldi|sprova4j|0.1.1|NA|pom" $EXPORT_FILE | wc -l)
if [[ $FIELDS -eq 1 ]]; then
    echo "PASS: file [$EXPORT_FILE] has sprova4j-0.1.1.pom."
else
    echo "FAIL: file [$EXPORT_FILE] has NOT sprova4j-0.1.1.pom."
    exit 20
fi

LOCK_FILE=$EXPORT_DIR/write.lock
if [[ ! -f $LOCK_FILE ]]; then
    echo "PASS: lock file [$LOCK_FILE] cleaned up."
else
    echo "FAIL: lock file [$LOCK_FILE] still present."
    exit 20
fi


PUBLISH_FILE=$PUBLISH_DIR/export.fld

if [[ -f $PUBLISH_FILE ]]; then
    echo "PASS: file [$PUBLISH_FILE] exists."
else
    echo "FAIL: file [$PUBLISH_FILE] does not exist."
    exit 20
fi

# Cleanup
rm -rf $EXPORT_DIR
cd $OLD_DIR
