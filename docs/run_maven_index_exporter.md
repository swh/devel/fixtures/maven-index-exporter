# Run Maven index exporter

## Running the full export

The `run_full_export.py` script located in `scripts/` provides an easy way to run the
export as a cron batch job, and copy the resulting text export to a specific location.


## Running the image only

The Docker image uses volumes to exchanges files. Prepare a directory with enough space
disk (see warning below) and pass it to docker:

```
$ LOCAL_DIR=/tmp/maven-index-exporter
# build the image
$ cd docker && docker build -f Dockerfile -t maven-index-exporter .
# run the image
$ docker run -v $LOCAL_DIR:/work maven-index-exporter
```

Please note that `LOCAL_DIR` *MUST* be an absolute path, as docker won't mount relative
paths as volumes.

For our purpose only the fld file is kept, so if you need other export files you should
simply edit the `extract_indexes.sh` script and comment the lines that do the cleaning.
Then rebuild the Docker image and run it.


## Running as cron

The `run_full_export.py` script located in `scripts/` provides an easy way to run the
export as a cron batch job, and copy the resulting text export to a specific location.

Simply use and adapt the crontab command as follows:

```
cd $HOME/maven-index-exporter/scripts/ && \
  ./myvenv/bin/python $HOME/maven-index-exporter/scripts/run_full_export.py \
    --base-url https://repo.maven.apache.org/maven2/ \
    2>&1 > /tmp/run_maven_exporter_$(date +"%Y%m%d-%H%M%S").log
```

Script usage:

```
$ python3 run_full_export.py  --help
Usage: run_full_export.py [OPTIONS]

Options:
  --base-url TEXT      Base url of the maven repository instance. Example:
                       https://repo.maven.apache.org/maven2/  [required]

  --work-dir TEXT      Absolute path to the temp directory.
  --publish-dir TEXT   Absolute path to the final directory.
  --docker-image TEXT  Docker image
  --help               Show this message and exit.
```

It is recommended to setup a virtual environment to run the script.

```
$ python3 -m venv myvenv
$ source venv/bin/activate
```

Python modules to be installed are provided in the `requirements.txt` file.
