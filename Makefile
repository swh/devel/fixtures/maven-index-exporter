VOLUME=maven_index_exporter
PUBLISH_DIR=/tmp/maven_index_exporter
# should be provided by the ci build
TAG=latest
IMAGE_NAME=softwareheritage/maven-index-exporter:$(TAG)
# ci build should provide the right label
IMAGE_LABEL=dummy
# can be overriden by ci
REGISTRY_URL=registry-1.docker.io

build:
	docker build \
		--tag $(IMAGE_NAME) \
		-f docker/Dockerfile \
		--label $(IMAGE_LABEL) \
		docker

prepare:
	docker volume create $(VOLUME)
	mkdir -p /tmp/maven_index_exporter

run: prepare
	docker run \
       -v $(VOLUME):/work \
       -v $(PUBLISH_DIR):/publish \
       $(IMAGE_NAME)

test:
	./tests/test_image.sh $(IMAGE_NAME)

clean:
	docker image ls -q --filter label=$(IMAGE_LABEL) \
		| uniq | xargs -r -t docker rmi --force

# This will only work through the ci
push:
	docker tag $(IMAGE_NAME) $(REGISTRY_URL)/$(IMAGE_NAME)
	docker push $(REGISTRY_URL)/$(IMAGE_NAME)
